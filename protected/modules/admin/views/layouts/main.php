
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/_all-skins.min.css">
    

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-2.2.3.min.js"></script>

    <title>
        <?php echo CHtml::encode($this->pageTitle); ?>
    </title>
</head>

<body class="hold-transition skin-blue sidebar-mini">
     <?php
        $layout_ctrl = Yii::app()->controller->id;    
        $layout_action = Yii::app()->controller->action->id;
    ?>

    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <span class="logo-lg"><b><?= Yii::app()->user->name?></b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>


        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" style="height: auto;">
            
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    	<?php if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin')):?>
	                        <li class="treeview <?php echo $layout_ctrl == 'user' ? 'active' : '' ;?>">
	                            <a href="/admin/user/manage">
	                                <i class="fa fa-dashboard"></i> <span>Users</span>
	                            </a>
	                        </li>
	                        <li class="treeview <?php echo $layout_ctrl == 'job' ? 'active' : '' ;?>">
	                            <a href="/admin/job/manage">
	                                <i class="fa fa-files-o"></i>
	                                <span>Job</span>
	                            </a>
	                        </li>
	                        <li class="treeview <?php echo $layout_action == 'load' && $layout_ctrl == 'assign'  ? 'active' : '' ;?>">
	                            <a href="/admin/assign/load">
	                                <i class="fa fa-th"></i> <span>Assign</span>
	                            </a>
                        	</li>
                            <li class="treeview <?php echo $layout_action == 'manage' && $layout_ctrl == 'assign' ? 'active' : '' ;?>">
                                <a href="/admin/assign/manage">
                                    <i class="fa fa-book"></i> <span>Job Assign</span>
                                </a>
                            </li>
                        <?php else:?>
                            <li class="treeview <?php echo $layout_action == 'manage' && $layout_ctrl == 'assign' ? 'active' : '' ;?>">
                                <a href="/admin/assign/manage">
                                    <i class="fa fa-book"></i> <span>Job Assign</span>
                                </a>
                            </li>
                        <?php endif;?>
                        <li>
                            <a href="/admin/default/logout">
                                <i class="fa fa-sign-out"></i><span>Logout</span>
                            </a>
                        </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <div class="content-wrapper" style="min-height: 916px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <?php echo $content; ?>
            </section>
            <!-- /.content -->
        </div>
    </div>
    <!-- page -->
    </div>
   <script type="text/javascript"> jQuery.noConflict(); </script>
   <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
</body>

</html>