<?php

class AssignController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/colum1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('manage','view','load','delete'),
				'roles'=>array('support'),
			),
			
		);
	}

	public function actionDelete($id = false)
	{
		$check = Yii::app()->user;
		if( $check->checkAccess('deleteAssign') )
		{
			Assign::model()->findByPk($id)->delete();
			$this->redirect('/admin/assign/load');
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	public function actionManage()
	{
		$check = Yii::app()->user;
		if( $check->checkAccess('manageAssign') )
		{
			$user_id = Yii::app()->user->id;

			$cri = new CDbCriteria();

			$cri->condition = "user_id = :user_id";

			$cri->params = array( ':user_id' => $user_id );

			$data = new CActiveDataProvider( 'Assign', array(
				'criteria' => $cri,
				'pagination' => array(
					'pageSize' => 5
				)

			));
			$this->render('manage',array(
				'data' => $data
			));
		}
		else
		{
			$this->redirect('/admin');
		}
		
	}

	public function actionView($id)
	{
		$check = Yii::app()->user;
		if( $check->checkAccess('viewAssign') )
		{
			$model = Assign::model()->findByPk($id);
			$this->render('view',array(
				'model' => $model
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	public function actionLoad()
	{
		$check = Yii::app()->user;
		if( $check->checkAccess('loadAssign') )
		{
			$user_id = Yii::app()->user->id;
			$data = Yii::app()->request;
			$model = new Assign('search');
			$model->unsetAttributes();
			if($data->getPost('Assign'))
			{
				$model->attributes = $data->getPost('Assign');
			}
			$this->render('load',array(
				'model' => $model
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	private function load_script( $action )
	{
		$client_script = Yii::app()->clientScript;

		if( $action == 'create' )
		{
			$client_script->registerCssFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.css'
			);

			$client_script->registerScriptFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.jquery.js', 
				CClientScript::POS_END
			);
			
			$client_script->registerScriptFile( 
				Yii::app()->request->baseUrl.'/js/jquery-2.2.3.min.js', 
				CClientScript::POS_HEAD
			);
		}
	}

}