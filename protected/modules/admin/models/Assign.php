<?php
Yii::import('application.modules.admin.models.base.BaseAssign');
class Assign extends BaseAssign
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    /*
    public function rules()
    {
        return CMap::mergeArray(
            parent::rules(),
            array(                
            )
        );
    }
    */
    
    /**
     * @return array relational rules.
     */
    
    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'job' => array(self::HAS_ONE, 'Job',array('id'=>'job_id')),
                'user' => array(self::HAS_ONE, 'User',array('id'=>'user_id')),
            )
        );
    }
    

    public function search()
    {
        $rs = parent::search();
        $rs->setPagination( array('pageSize' => 5) );
        return $rs;
    }

}

?>