
<?php

class DefaultController extends Controller
{
	public $layout='/layouts/colum';
	public function actionIndex()
	{
		if(Yii::app()->user->isGuest)
		{
			$this->redirect('/admin/default/login');
			
		}else{
			if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin') )
			{
				$this->redirect('/admin/user/manage');
			}
			else
			{
				$this->redirect('/admin/assign/manage');
			}
		}
	}
	public function actionLogin()
	{
		$this->layout=false;
		$model=new LoginForm;
		$post = Yii::app()->request;
		// if it is ajax validation request
		if($post->getPost('ajax') && $post->getPost('ajax')==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		$user = Yii::app()->user;
		// collect user input data
		if($user->isGuest)
		{
			if($post->getPost('LoginForm'))
			{
				$model->attributes=$post->getPost('LoginForm');
				// validate user input and redirect to the previous page if valid
				if($model->validate() && $model->login())
				{
					if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin') )
					{
						$this->redirect('/admin/user/manage');
					}
					else
					{
						$this->redirect('/admin/assign/manage');
					}
				}
			}
			// display the login form
			$this->render('login',array('model'=>$model));
		}else{
			if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin'))
			{
				$this->redirect('/admin/user/manage');
			}
			else
			{
				$this->redirect('/admin/assign/manage');
			}
		}
		
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('/admin/default/login');
	}

	
}