<?php
if(Yii::app()->user->checkAccess('super admin'))
{
	$this->menu=array(
		array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'DeActive User', 'url'=>array('delete', 'id'=>$model->id)),
		array('label'=>'Manage User', 'url'=>array('manage')),
	);
}
else
{
	$this->menu=array(
		array('label'=>'Manage User', 'url'=>array('manage')),
	);
}
?>

<h1>View User</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'email',
		'phone',
		'photo',
		'job',
	),
)); ?>
