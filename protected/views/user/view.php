<?php
/* @var $this UserController */
/* @var $model BaseUser */

$this->breadcrumbs=array(
	'Base Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List BaseUser', 'url'=>array('index')),
	array('label'=>'Create BaseUser', 'url'=>array('create')),
	array('label'=>'Update BaseUser', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BaseUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BaseUser', 'url'=>array('admin')),
);
?>

<h1>View BaseUser #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'email',
		'phone',
		'photo',
		'job',
		'password',
		'type',
		'status',
	),
)); ?>
