<?php
/* @var $this JobController */
/* @var $model BaseJob */
/* @var $form CActiveForm */
// foreach ($user as $key => $value) {
// 	var_dump($value->id);
// }die;

?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'base-job-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
                'validateOnSubmit'=>true,
        ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>


	<div class="row">
		<div class="col-lg-2">
			<?php
				echo CHtml::dropDownList('Assign[user_id][]', '', 
	          	CHtml::listData($user, 'id', 'name'),
	          	array('empty' => '(Select a category', 'class'=>'chosen','multiple'=>'true'));
			?>
		</div>
	</div>

	
	<div class="btn buttons">
		<?php echo CHtml::submitButton('Create',['class' => 'btn btn-success']); ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">

    $(document).ready(function(){    	
		$(".chosen").chosen();
	});
</script>
</div>