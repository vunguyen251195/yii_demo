<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/square/blue.css">
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php $form=$this->beginWidget('CActiveForm', array(
          	'id'=>'login-form',
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
	       )); 
  ?>
     <div class="form-group has-feedback">
		<?php echo $form->textField($model,'username',array('class'=>'form-control', 'placeholder'=>'username')); ?>
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		<?php echo $form->error($model,'username'); ?>
      </div>
      <div class="form-group has-feedback">
		<?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'password')); ?>
		<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		<?php echo $form->error($model,'password'); ?>
		
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary btn-block btn-flat')); ?>
        </div>
      </div>
    <?php $this->endWidget(); ?>

  </div>
</div>

</body>
</html>