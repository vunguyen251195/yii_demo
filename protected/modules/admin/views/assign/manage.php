<h1>Manage Assign</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'base-user-grid',
	'htmlOptions' => array('class' => 'grid-view table-responsive'),
	'dataProvider'=> $data,
	'columns'=>array(
		array(
                'header' => '#',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
		array(
			'name'        => 'Assign.Job',
			'value' => '$data->job->name',
		),
		array(
			'class'=>'CButtonColumn',
			'template' =>'{view}'
		),
	),
)); ?>