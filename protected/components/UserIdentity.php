<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

	private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->findByAttributes(array('username'=>$this->username));

		if ($user===null) 
		{ // No user found!
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} else if ( !CPasswordHelper::verifyPassword($this->password, $user->password) ) 
		{ // Invalid password!
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		} 
		else 
		{ // Okay!			
		    // Store the role in a session:
			$this->_id = $user->id;
			 $this->setState('status', $user->status);
			 $this->setState('type', $user->type);
			 $this->setState('photo', $user->photo);
		    $role = Roles::model()->findByPk($user->type);
		    $this->setState('role', $role->description);
		    $this->errorCode=self::ERROR_NONE;
		    
		}
		return !$this->errorCode;
	}
	
	public function getId()
	{
	 return $this->_id;
	}
}