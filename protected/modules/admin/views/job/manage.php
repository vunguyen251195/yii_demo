

<h1>Manage Jobs</h1>
<a href="/admin/job/create" class="btn btn-success">Create Job</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'base-job-grid',
	'htmlOptions' => array('class' => 'grid-view table-responsive'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                'header' => '#',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
		'name',
		'title',
		'due_date',
		'details',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
