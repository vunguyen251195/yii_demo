<h1>Manage Users</h1>
<?php 
	if(in_array('createUser', $ar) || Yii::app()->user->checkAccess('super admin')):
?>
	<a href="/admin/user/create" class="btn btn-success">Create User</a>
<?php
	else:
?>

<?php
	endif;
?>
<?php

 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'base-user-grid',
	'htmlOptions' => array('class' => 'grid-view table-responsive'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	
	'columns'=>array(
		array(
                'header' => '#',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
		'name',
		'email',
		'phone',
		'job',
		array(
			'name'        => 'User.description ',
			'value' => '$data->role->description',
		),
		array(
			'class'=>'CButtonColumn',
			'template' => Yii::app()->user->checkAccess('super admin')? '{view} {update} {delete}':'{view}'
			
		),
	),
)); ?>
