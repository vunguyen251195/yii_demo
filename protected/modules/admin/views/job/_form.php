

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'base-job-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	        'validateOnSubmit'=>true,
	    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name',array('class' => 'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'col-xs-3 col-sm-3 col-md-2','style' => 'padding: 0')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<br>
	<div class="row ">
		<?php echo $form->labelEx($model,'title',array('class' => 'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50,'class' => 'col-xs-3 col-sm-3 col-md-2','style' => 'padding: 0')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
	<br>
	<div class="row">
		<?php echo $form->labelEx($model,'due_date',array('class' => 'col-lg-1 col-xs-2')); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'Job[due_date]',
                            	'value'=>Yii::app()->dateFormatter->format("y-M-d",strtotime($model->due_date)),
                                'options'=>array(
                                'dateFormat' => 'yy-mm-dd',
                                'showAnim'=>'fold',
                                'minDate' => 'today',
                                ),
                                'htmlOptions'=>array(
                                'style'=>'height:20px;'
                                ),
                        ));  ?>
		<?php echo $form->error($model,'due_date'); ?>
	</div>
	<br>
	<div class="row">
		<div class="col-lg-1 col-xs-2">
			<label>User*</label>
		</div>
		<div class="col-xs-3 col-sm-3 col-md-2" style="padding:0;">
			<?php
				if($model->isNewRecord == 'Create')
				{
					echo CHtml::dropDownList('Assign[user_id][]', '', 
		          	CHtml::listData($user, 'id', 'name'),
		          	array(
		          		'empty' => '(Select a category',
		          		'class'=>'chosen',
		          		'multiple'=>'true'
		          	));
				}
				else{
					$selectedOptions = [];

					foreach ( $assign as $as_vl)
					{
						$selectedOptions[$as_vl] = array( 'selected'=> true );					
					}

					echo CHtml::dropDownList('Assign[user_id][]', '', 
		          	CHtml::listData($user, 'id', 'name'),
		          	array(
		          		'empty' => '(Select a category',
		          		'class'=>'chosen',
		          		'multiple'=>'true',
		          		'options'=> $selectedOptions
		          	));
				}
				
			?>
		</div>
		<?php if(Yii::app()->user->hasFlash('unsuccess'))
					echo Yii::app()->user->getFlash('unsuccess');
		?>
	</div>
	<script type="text/javascript">
		 jQuery(document).ready(function(){    	
		jQuery(".chosen").chosen();
	});
	</script>
	<br>
	<div class="row">
		<?php echo $form->labelEx($model,'details',array('class' => 'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textArea($model,'details',array('size'=>60,'maxlength'=>255,'class' => 'col-xs-3 col-sm-3 col-md-2','style' => 'padding: 0')); ?>
		<?php echo $form->error($model,'details'); ?>
	</div>
	<br>
	<div class="btn buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success')); ?>
		<a href="/admin/job/manage" class="btn btn-danger">Cancel</a>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->