<?php
Yii::import('application.modules.admin.models.base.BaseJob');
class Job extends BaseJob
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    /*
    public function rules()
    {
        return CMap::mergeArray(
            parent::rules(),
            array(                
            )
        );
    }
    */
    
    /**
     * @return array relational rules.
     */
    
    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'assign' => array(self::HAS_MANY, 'job_id', 'id'),
            )
        );
    }
    

    public function search()
    {
        $rs = parent::search();
        $rs->setPagination( array('pageSize' => 5) );
        return $rs;
    }
    
}

?>