<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/colum1';

	/**
	 * @return array action filters
	 */

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete',
		);
	}
	

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','manage','delete'),
				'roles'=>array('support'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$data = Yii::app()->user;
		if($data->checkAccess('viewUser'))
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionCreate()
	{
		$data = Yii::app()->user;
		$t = Temp::model()->findAll(array(
						'condition' => 'user_id = :user_id',
		          		'params'    => array(':user_id' => $data->id)
					));
		$art = [];
		foreach ($t as $value) {
			$art[] = $value->auth_item_child_id;
		}
		$a = AuthItemChild::model()->findAllByAttributes(array(
						'id'=>$art
					));
		$ar = [];
		foreach ($a as $value) {
			$ar[] = $value->child;
		}
		
		
		if(in_array('createUser', $ar) || Yii::app()->user->checkAccess('super admin'))
		{
				$model=new User;
					$post = Yii::app()->request->getPost('User');
					$authitem = AuthItem::model()->findAll(array(
							'condition' => 'type <> :type',
			          		'params'    => array(':type' => 2)
						));
					if($post)
					{
						$model->attributes=$post;
						$model->photo=CUploadedFile::getInstance($model,'photo');
						$model->password = CPasswordHelper::hashPassword($model->password);

						if($model->save())
						{
							if(!empty($model->photo))
							{

				                $rep = Yii::app()->params['webroot'].'upload/';

				                $model->photo->saveAs($rep.$model->photo);

			            	}

							$auth = new AuthAssignment;
							$auth->userid = $model->id;
							$auth->itemname = Roles::model()->findByPk($model->type)->description;
							$auth->save();

							$post = Yii::app()->request->getPost('AuthItemChild');

							if( !empty( $post['child'] ) )
							{
								foreach ($post['child'] as $vl) 
								{
									$authitemchild = AuthItemChild::model()->findAll(array(
										'condition' => 'child = :child',
						          		'params'    => array(':child' => $vl)
									));
									foreach ($authitemchild as $value) {
										$id = $value->id;
									}
									$temp = new Temp;				
									$temp->user_id = $model->id;
									$temp->auth_item_child_id = $id;
									if( !$temp->save() )
									{
									}
								}
							}
							$this->redirect(array('view','id'=>$model->id));
						}
					}
					$this->load_script( 'create' );
					$this->render('create',array(
						'model'=>$model, 'auth'=>$authitem,'arr'=>$ar
					));
		}		
		
		else
		{
			$this->redirect('/admin');
		}
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$data = Yii::app()->user;
		$t = Temp::model()->findAll(array(
						'condition' => 'user_id = :user_id',
		          		'params'    => array(':user_id' => $data->id)
					));
		$art = [];
		foreach ($t as $value) {
			$art[] = $value->auth_item_child_id;
		}
		$a = AuthItemChild::model()->findAllByAttributes(array(
						'id'=>$art
					));
		$ar = [];
		foreach ($a as $value) {
			$ar[] = $value->child;
		}
		if($data->checkAccess('super admin') || in_array('editUser', $ar))
		{

			$model=$this->loadModel($id);

			$post = Yii::app()->request->getPost('User');
			$authitem = AuthItem::model()->findAll(array(
							'condition' => 'type <> :type',
			          		'params'    => array(':type' => 2)
						));
			$t1 = Temp::model()->findAll(array(
						'condition' => 'user_id = :user_id',
		          		'params'    => array(':user_id' => $id)
					));
			$art1 = [];
			foreach ($t1 as $value) {
				$art1[] = $value->auth_item_child_id;
			}
			$a1 = AuthItemChild::model()->findAllByAttributes(array(
							'id'=>$art1
						));
			$ar1 = [];
			foreach ($a1 as $value) {
				$ar1[] = $value->child;
			}	
			if($post)
			{
				$model->attributes=$post;
				$model->photo=CUploadedFile::getInstance($model,'photo');
				if($model->save())
				{
					if(!empty($model->photo))
					{

		                $rep = Yii::app()->params['webroot'].'upload/';

		                $model->photo->saveAs($rep.$model->photo);

	            	}
	            	$auth = AuthAssignment::model()->find(array(
	            		'condition' => 'userid = :userid',
	          			'params'    => array(':userid' => $id)
	            	));
	            	$auth->itemname = Roles::model()->findByPk($model->type)->description;
	            	$auth->save();

	            	

		            $post = Yii::app()->request->getPost('AuthItemChild');
							if( !empty( $post['child'] ) )
							{
								Temp::model()->deleteAll(array(
										'condition' => 'user_id = :user_id',
										'params' => array(':user_id' => $id)
									));
								$arr_child = [];
								foreach ($post['child'] as $vl) 
								{
									$authitemchild = AuthItemChild::model()->findAll(array(
										'condition' => 'child = :child',
						          		'params'    => array(':child' => $vl)
									));
									foreach ($authitemchild as $value) {
										$id = $value->id;
									}
									$temp = new Temp;				
									$temp->user_id = $model->id;
									$temp->auth_item_child_id = $id;
									if( !$temp->save() )
									{
									}
								}
							}

					$model->save();
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			$this->load_script( 'create' );
			$this->render('update',array(
				'model'=>$model,'auth'=>$authitem, 'ar'=>$ar1
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = Assign::model()->findAll(array(
                      'condition' => 'user_id = :user_id',
                      'params'    => array(':user_id' => $id)
                  ));
		foreach($model as $vl) 
		{ 
			$vl->delete();
		}
		$this->loadModel($id)->delete();
		$request = Yii::app()->request;
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!$request->getParam('ajax'))
			$this->redirect('/admin/user/manage');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionManage()
	{
		$t = Temp::model()->findAll(array(
						'condition' => 'user_id = :user_id',
		          		'params'    => array(':user_id' => Yii::app()->user->id)
					));
		$art = [];
		foreach ($t as $value) {
			$art[] = $value->auth_item_child_id;
		}
		$a = AuthItemChild::model()->findAllByAttributes(array(
						'id'=>$art
					));
		$ar = [];
		foreach ($a as $value) {
			$ar[] = $value->child;
		}
		if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin'))
		{
			$get = Yii::app()->request->getParam('User');
			$model=new User('search');
			$model->unsetAttributes();
			if($get)
				$model->attributes=$get;

			$this->render('manage',array(
				'model'=>$model,'ar'=>$ar
			));
		}
		else
		{
			$this->redirect('/admin/assign/manage');
		}
		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BaseUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BaseUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='base-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function load_script( $action )
	{
		$client_script = Yii::app()->clientScript;

		if( $action == 'create' || $action == 'update' )
		{
			$client_script->registerCssFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.css'
			);

			$client_script->registerScriptFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.jquery.js', 
				CClientScript::POS_END
			);
			
			// $client_script->registerScriptFile( 
			// 	Yii::app()->request->baseUrl.'/js/jquery-2.2.3.min.js', 
			// 	CClientScript::POS_HEAD
			// );
		}
	}
}
