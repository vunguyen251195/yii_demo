<?php

class BackendController extends Controller
{
	public function beforeAction( $action )
	{
		if($action->id == 'create')
		{
			if(Yii::app()->user->checkAccess('createUser'))
				return true;
			else
			    return false;
		}
		
	}
}