<?php
Yii::import('application.modules.admin.models.base.BaseUser');
class User extends BaseUser
{
    const TYPE_ADMIN = 1;
    const TYPE_SUPPORT = 2;

    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 10;
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    
    public function rules()
    {
        return CMap::mergeArray(
            parent::rules(),
            array( 
                array('photo', 'file',
                    'types'=>'jpg, gif, png',
                    'maxSize'=>1024 * 1024 * 50,    // 50 MB
                    'allowEmpty'=>true,
                    'safe'=>true),
            )

        );
    }
    
    
    /**
     * @return array relational rules.
     */
    
    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'role' => array(self::HAS_ONE, 'Roles',array('id'=>'type')),
            )
        );
    }
    

    public function search()
    {
        $rs = parent::search();
        $rs->setPagination( array('pageSize' => 5) );
        return $rs;
    }
    
}

?>