<?php
/* @var $this UserController */
/* @var $model BaseUser */
/* @var $form CActiveForm */
?>
<div class="form">

<?php 

	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'base-user-form',
		'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'clientOptions'=>array(
	        'validateOnSubmit'=>true,
	    ),
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>


	<div class="row">
		<?php echo $form->labelEx($model,'name',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50,'class'=>'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'name',array('style'=>'color:red;')); ?>

		<?php echo $form->labelEx($model,'email',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50,'class'=>'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'email',array('style'=>'color:red;')); ?>
	</div>
	<br>
	<div class="row">
		<?php echo $form->labelEx($model,'phone',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'phone',array('class'=> 'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'phone',array('style'=>'color:red;')); ?>

		<?php echo $form->labelEx($model,'photo',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->fileField($model,'photo',array('class'=> 'col-xs-4 col-sm-3 col-md-2','style'=>' padding: 0;')); ?>
		<?php echo $form->error($model,'photo',array('style'=>'color:red;')); ?>
	</div>
	
	<br>
	<?php
		if($model->isNewRecord == 'Create'):
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'username',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'username',array('size'=>50,'maxlength'=>50,'class'=>'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'username',array('style'=>'color:red;')); ?>

		<?php echo $form->labelEx($model,'password',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255,'class'=>'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'password',array('style'=>'color:red;')); ?>
	</div>
	<br>
	<?php
		endif;
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'job',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->textField($model,'job',array('size'=>50,'maxlength'=>50,'class'=>'col-xs-4 col-sm-3 col-md-2','style'=>'padding:0')); ?>
		<?php echo $form->error($model,'job',array('style'=>'color:red;')); ?>

		<?php echo $form->labelEx($model,'type',array('class'=>'col-lg-1 col-xs-2')); ?>
		<?php echo $form->dropDownList($model,'type',CHtml::listData(Roles::model()->findAll(),'id','description'),['class'=>'col-xs-4 col-sm-3 col-md-2', 'options' => array('2'=> array('selected'=>true))]); ?>
		<?php echo $form->error($model,'type',array('style'=>'color:red;')); ?>
	</div>
	<div class="row">
		<?php

			if($model->isNewRecord == 'Create')
				{
					echo CHtml::dropDownList('AuthItemChild[child][]', '', 
		          	CHtml::listData($auth, 'name', 'name'),
		          	array(
		          		'empty' => '(Select a category',
		          		'class'=>'chosen col-xs-4 col-sm-3 col-md-2',
		          		'multiple'=>'true'
		          	));
				}
				else{
					$selectedOptions = [];

					foreach ( $ar as $as_vl)
					{
						$selectedOptions[$as_vl] = array( 'selected'=> true );					
					}

					echo CHtml::dropDownList('AuthItemChild[child][]', '', 
		          	CHtml::listData($auth, 'name', 'name'),
		          	array(
		          		'empty' => '(Select a category',
		          		'class'=>'chosen',
		          		'multiple'=>'true',
		          		'options'=> $selectedOptions
		          	));
				}
		?>
	</div>

	<script type="text/javascript">
		 jQuery(document).ready(function(){    	
		jQuery(".chosen").chosen();
	});
	</script>

	<div class="btn row">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success')); ?>
		<a href="/admin/user/manage" class="btn btn-danger">Cancel</a>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->