<?php
/* @var $this JobController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Base Jobs',
);

$this->menu=array(
	array('label'=>'Create BaseJob', 'url'=>array('create')),
	array('label'=>'Manage BaseJob', 'url'=>array('manage')),
);
?>

<h1>Base Jobs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
