<?php

class JobController extends Controller
{
	public $layout='/layouts/colum1';
	
	/**
	 * @return array action filters
	 */
	
	public function filters()
	{
	    return array(
	        'accessControl', // perform access control for CRUD operations
	    );
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','manage','delete','create','update'),
				'roles'=>array('support'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$check = Yii::app()->user;
		if($check->checkAccess('viewJob'))
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		else
		{
			$this->redirect('/admin');
		}
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$check = Yii::app()->user;
		$t = Temp::model()->findAll(array(
						'condition' => 'user_id = :user_id',
		          		'params'    => array(':user_id' => $check->id)
					));
		$art = [];
		foreach ($t as $value) {
			$art[] = $value->auth_item_child_id;
		}
		$a = AuthItemChild::model()->findAllByAttributes(array(
						'id'=>$art
					));
		$ar = [];
		foreach ($a as $value) {
			$ar[] = $value->child;
		}
		if($check->id)
		{
			if(in_array('createJob', $ar))
			{
				$model = new Job;
				$user = User::model()->findAll();
				$data = Yii::app()->request->getPost('Job');

				if( $data )
				{
					$model->attributes = $data;

					if($model->save())
					{
						$post = Yii::app()->request->getPost('Assign');
						if( !empty( $post['user_id'] ) )
						{
							$validate = true;
							foreach ($post['user_id'] as $vl) 
							{
								$model_as = new Assign;				
								$model_as->job_id = $model->id;
								$model_as->user_id = $vl;
								if( !$model_as->save() )
								{
									$validate = false;	
								}

							}
						}
						if( empty( $post['user_id'] ) )
						{
							Yii::app()->user->setFlash('unsuccess','user cannot be blank');
						}
						
						if( $validate == true )
						{
							$this->redirect(array('view','id'=>$model->id));
						}
						
					}
					
				}
				$this->load_script( 'create' );
				$this->render('create',array(
					'model'=>$model,'user'=>$user
				));
			}
			else
			{
				$this->redirect('/admin');
			}
		}
		else
		{
			$this->redirect('/admin');
		}
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$check = Yii::app()->user;
		if($check->checkAccess('editJob'))
		{
			$model= $this->loadModel( $id );
			$ass = Assign::model()->findAll(array(
			  'select' => 'user_id',	
	          'condition' => 'job_id = :job_id',
	          'params'    => array(':job_id' => $id)
	        ));

			$arr = [];
			foreach ( $ass as $vl) {
				$arr[] = $vl->user_id;
			}
			$user = User::model()->findAll();

			$data = Yii::app()->request->getPost('Job');

			if($data)
			{
				$model->attributes = $data;
				if($model->save())
				{
					$post = Yii::app()->request->getPost('Assign');
					if( !empty( $post['user_id'] ) )
					{

						$validate = true;
						if($post['user_id'] != $arr)
						{
							foreach ($post['user_id'] as $vl) 
							{
								$model_as = new Assign;
								$model_as->job_id = $model->id;
								$model_as->user_id = $vl;
									
								if( !$model_as->save() )
								{
									$validate = false;	
								}
							}	
						}
						
					}
					
					if( $validate == true )
					{
						$this->redirect(array('view','id'=>$model->id));
					}
				}
			}
			$this->load_script('update');
			$this->render('update',array(
				'model'=>$model,'user'=>$user, 'assign' => $arr
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id = false)
	{
		$check = Yii::app()->user;
		if($check->checkAccess('deleteJob'))
		{
			$model = Assign::model()->findAll(array(
	                      'condition' => 'job_id = :job_id',
	                      'params'    => array(':job_id' => $id)
	                  ));
			foreach($model as $model1) 
			{ 
				$model1->delete();
			}
			
			$this->loadModel($id)->delete();
			$request = Yii::app()->request;
			if(!$request->getParam('ajax'))
			{
				$this->redirect('/admin/job/manage');
			}
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Job');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionManage()
	{
		$check = Yii::app()->user;
		if(Yii::app()->user->checkAccess('super admin') || Yii::app()->user->checkAccess('admin'))
		{
			$model=new Job('search');
			$data = Yii::app()->request->getPost('Job');
			$model->unsetAttributes();  // clear any default values
			if($data)
				$model->attributes=$data;

			$this->render('manage',array(
				'model'=>$model,
			));
		}
		else
		{
			$this->redirect('/admin');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BaseJob the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Job::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BaseJob $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='base-job-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function load_script( $action )
	{
		$client_script = Yii::app()->clientScript;

		if( $action == 'create' || $action == 'update' )
		{
			$client_script->registerCssFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.css'
			);

			$client_script->registerScriptFile( 
				Yii::app()->request->baseUrl.'/themes/plugins/chosen/chosen.jquery.js', 
				CClientScript::POS_END
			);
			
			// $client_script->registerScriptFile( 
			// 	Yii::app()->request->baseUrl.'/js/jquery-2.2.3.min.js', 
			// 	CClientScript::POS_HEAD
			// );
		}
	}
}
